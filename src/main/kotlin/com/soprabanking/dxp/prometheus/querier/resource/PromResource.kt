package com.soprabanking.dxp.prometheus.querier.resource

import com.soprabanking.dxp.commons.constants.logger
import com.soprabanking.dxp.prometheus.model.api.MeasureDTO
import com.soprabanking.dxp.prometheus.querier.client.PrometheusClient
import com.soprabanking.dxp.prometheus.querier.service.MetricService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono
import reactor.core.publisher.zip
import java.time.OffsetDateTime
import java.time.temporal.ChronoUnit
import kotlin.math.round
import kotlin.math.roundToLong

/**
 * REST resource publishing [Metrics] endpoints
 */
@RestController
@RequestMapping("api/metrics")
class PromResource(private val client: PrometheusClient, private val service: MetricService) {

    companion object {
        const val URI = "/api/metrics"
        val logger = logger()
    }

    @GetMapping("/{namespace}")
    fun findAll(@PathVariable namespace: String, @RequestParam("start") startValue: Long, @RequestParam("end") end: Long): Mono<String> {
        val start = OffsetDateTime.now().minus(startValue, ChronoUnit.MINUTES)
        return service.findServices(start, namespace)
                .filter { !listOf("busybox", "sleep", "ui-backoffice").contains(it) }
                .flatMap { it.toMeasure(start, namespace, end) }
                .sort(Comparator<MeasureDTO> { a, b -> if (a.msName == "server-proxy") 1 else if (b.msName == "server-proxy") -1 else 0 }
                              .then(compareBy { it.msName }))
                .doOnNext{logger.info("$it")}
                .map {
                    "|${it.msName}|${it.cpu.round(3)}|2|${it.ops.round(1)}|${it.p50ms.roundToLong()}|${it.p90ms.roundToLong()}|${it.p99ms.roundToLong()}|${it.successPercent.round(
                            2
                    )}| | |\n"
                }
                .doOnNext{logger.info(it)}
                .reduce("||Microservice||vCPU||Instances||Op/s||P50(ms)||P90(ms)||P99(ms)||Success(%)||DB size(Mo)||Comments||\n", String::plus)
                .doOnSuccess { println(it) }
    }

    private fun String.toMeasure(start: OffsetDateTime, namespace: String, end: Long) =
            listOf(
                    this.findPercentile(50f, start, namespace, end),
                    this.findPercentile(90f, start, namespace, end),
                    this.findPercentile(99f, start, namespace, end),
                    this.findOps(start, namespace, end),
                    this.findCPU(start, namespace, end),
                    this.findSuccess(start, namespace, end)
            ).zip { (p50, p90, p99, ops, cpu, success) -> MeasureDTO(this, cpu, ops, p50, p90, p99, success) }

    private fun String.findPercentile(percent: Float, start: OffsetDateTime, namespace: String, end: Long) =
            service.findPercentile(this, percent, start, start.plus(end, ChronoUnit.MINUTES), 5, namespace)

    private fun String.findOps(start: OffsetDateTime, namespace: String, end: Long) =
            service.findOps(this, start, start.plus(end, ChronoUnit.MINUTES), 5, namespace)

    private fun String.findCPU(start: OffsetDateTime, namespace: String, end: Long) =
            service.findCPU(this, start, start.plus(end, ChronoUnit.MINUTES), 5, namespace)

    private fun String.findSuccess(start: OffsetDateTime, namespace: String, end: Long) =
            service.findSuccess(this, start, start.plus(end, ChronoUnit.MINUTES), 5, namespace)

    operator fun <T> List<T>.component6() = this[5]

    fun Double.round(decimals: Int): Double {
        var multiplier = 1.0
        return takeIf { !isNaN() }?.let {  repeat(decimals) { multiplier *= 10 }
         round(this * multiplier) / multiplier } ?: 0.0
    }
}