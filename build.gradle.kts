import com.soprabanking.dxp.*

dxpKotlinApplication{
    apiVersion = 1
}
dependencies {
	implementation(dxp("api"))
	implementation(dxp("client"))
	runtimeOnly(dxp("monitor"))
	testImplementation(dxp("test"))
    
}

tasks.withType<Test>().configureEach { failFast = false }
